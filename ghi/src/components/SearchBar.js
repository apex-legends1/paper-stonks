import "../App.css";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function SearchBar({ setSymbol, symbol }) {
    const navigate = useNavigate();
    const [ticker, setTicker] = useState("");
    const [searchResults, setSearchResults] = useState([]);
    const searchTicker = async (ticker) => {
        const requestOptions = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                ticker: ticker,
            }),
        };

        const response = await fetch(
            `${process.env.REACT_APP_TRADING_HOST}/search`,
            requestOptions
        );
        if (response.ok) {
            const data = await response.json();

            setSearchResults(data["tickers"]);
        }
    };

    useEffect(() => {
        if (ticker) {
            searchTicker(ticker);
        } else {
            setSearchResults([]);
        }
    }, [setTicker, ticker]);

    return (
        <div className="search-bar-dropdown overflow-auto">
            <form
                className=""
                onSubmit={(e) => {
                    e.preventDefault();
                    e.target.reset();
                    navigate(`/stock/${symbol.toUpperCase()}`);
                }}
            >
                <input
                    className="form-control searchbar"
                    type="text"
                    placeholder="Stock search"
                    aria-label="Stock search"
                    onChange={(e) => {
                        setTicker(e.target.value);
                    }}
                    value={ticker}
                />
                <div className="list-group position-fixed">
                    {searchResults &&
                        searchResults.map((ticker, index) => {
                            return (
                                <button
                                    type="button"
                                    className="list-group-item list-group-item-action searchbar overflow-visible"
                                    aria-current="true"
                                    onClick={(e) => {
                                        navigate(`/stock/${e.target.value}`);
                                        setTicker("");
                                        setSymbol(e.target.value);
                                        setSearchResults([]);
                                    }}
                                    key={index}
                                    value={ticker["1. symbol"]}
                                >
                                    {ticker["1. symbol"] +
                                        " (" +
                                        ticker["2. name"].substring(0, 10) +
                                        "...)"}
                                </button>
                            );
                        })}

                    {/* <button
                        type="button"
                        className="list-group-item list-group-item-action searchbar"
                        disabled
                    >
                        A disabled item
                    </button> */}
                </div>
            </form>
        </div>
    );
}

export default SearchBar;
