from fastapi import APIRouter, Depends, Response
from typing import Union
from queries.alpha_vantage import (
    StockDataIn,
    StockDataOut,
    StockDataRepository,
    Error,
    ArticleDataOut,
    ArticleDataIn,
    SearchResultIn,
    SearchResultOut,
)

router = APIRouter()


@router.post("/stock", response_model=Union[StockDataOut, Error])
def get_data_points(
    stock: StockDataIn,
    response: Response,
    repo: StockDataRepository = Depends(),
):
    return repo.get_data_points(stock)


@router.post("/news", response_model=Union[ArticleDataOut, Error])
def get_articles(
    stock: ArticleDataIn,  # AAPL
    response: Response,
    repo: StockDataRepository = Depends(),
):
    return repo.get_articles(stock)


@router.post("/search", response_model=Union[SearchResultOut, Error])
def search_ticker(
    ticker: SearchResultIn,
    response: Response,
    repo: StockDataRepository = Depends(),
):

    return repo.get_search_results(ticker)
