import requests
import os
from pydantic import BaseModel

from typing import Union

alpha_vantage = os.environ.get("REACT_APP_ALPHA_VANTAGE")


class Error(BaseModel):
    message: str


class StockDataIn(BaseModel):
    symbol: str
    interval: str


class StockDataOut(BaseModel):
    labels: list
    points: list


class ArticleDataIn(BaseModel):
    symbol: str  # AAPL


class ArticleDataOut(BaseModel):
    articles: list


class SearchResultIn(BaseModel):
    ticker: str


class SearchResultOut(BaseModel):
    tickers: list


class StockDataRepository:
    def get_data_points(
        self, stock: StockDataIn
    ) -> Union[Error, StockDataOut]:
        response = requests.get(
            f"https://www.alphavantage.co/query?function=TIME_SERIES_"
            f"{stock.interval}&symbol={stock.symbol}"
            f"&apikey={alpha_vantage}"
        )
        data = response.json()
        if stock.interval == "DAILY":
            time_series = data.get("Time Series (Daily)")
        elif stock.interval == "MONTHLY":
            time_series = data.get("Monthly Time Series")
        elif stock.interval == "WEEKLY":
            time_series = data.get("Weekly Time Series")
        else:
            return Error(message="invalid time series")

        labels = list([x[5:] for x in time_series.keys()])
        quotes = list(time_series.values())
        points = []

        for quote in quotes:
            points.append(quote.get("4. close"))
        return StockDataOut(labels=labels, points=points)

    def get_articles(
        self, article: ArticleDataIn
    ) -> Union[Error, ArticleDataOut]:
        symbol = article.symbol
        print(symbol)
        response = requests.get(
            f"https://www.alphavantage.co/query?function=NEWS_SENT"
            f"IMENT&tickers={symbol}&apikey={alpha_vantage}"
        )
        data = response.json()
        print(data)
        # print("\n\ndata: ",data)
        articles = data.get("feed")
        result = []

        if articles:

            # print("Symbol: ", symbol)

            for x in articles:
                if len(result) < 5:
                    if symbol in x["title"]:
                        dash_index = x["title"].find(" -")
                        title = x["title"][0:dash_index]
                        dict = {
                            "title": title,
                            "url": x["url"],
                            "source": x["source"],
                            "banner_image": x["banner_image"],
                            "time_published": x["time_published"][4:6]
                            + "/"
                            + x["time_published"][6:8],
                        }
                        result.append(dict)
                else:
                    break

        return ArticleDataOut(articles=result)

    def get_search_results(
        self, ticker: SearchResultIn
    ) -> Union[Error, SearchResultOut]:
        symbol = ticker.ticker
        print("Symbol: ", symbol)
        response = requests.get(
            f"https://www.alphavantage.co/query?function=SYMBOL_SEARCH"
            f"&keywords={symbol}&apikey={alpha_vantage}"
        )

        data = response.json()

        if data["bestMatches"]:
            results = SearchResultOut(tickers=data["bestMatches"])
        else:
            results = Error(message="no results")
        print(results)
        return results
